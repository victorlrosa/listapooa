/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.victor.listaWeb.models;

import br.victor.listaWeb.enums.ETipoCarro;
import java.util.ArrayList;

/**
 *
 * @author Victor
 */
public class Carro {

    private String modelo;
    private Double preco;
    private ETipoCarro tipo;
    private Boolean ar, cambioAutomatico, alarme, pinturaEspecial, pinturaMetalica,
            pinturaComemorativa, tetoSolar, kitMultimidia, motor1x0, motor1x6, motor2x0, isImportado = false;

    public Carro() {
    }

    public Carro(String modelo, ETipoCarro tipo, Boolean ar, Boolean cambioAutomatico,
            Boolean alarme, Boolean pinturaEspecial, Boolean pinturaMetalica, 
            Boolean pinturaComemorativa, Boolean tetoSolar, Boolean kitMultimidia,
            Boolean motor1x0, Boolean motor1x6, Boolean motor2x0) {
        this.modelo = modelo;
        this.tipo = tipo;
        this.ar = ar;
        this.cambioAutomatico = cambioAutomatico;
        this.alarme = alarme;
        this.pinturaEspecial = pinturaEspecial;
        this.pinturaMetalica = pinturaMetalica;
        this.pinturaComemorativa = pinturaComemorativa;
        this.tetoSolar = tetoSolar;
        this.kitMultimidia = kitMultimidia;
        this.motor1x0 = motor1x0;
        this.motor1x6 = motor1x6;
        this.motor2x0 = motor2x0;
    }

    public double getPreco() {
        switch (getTipo()) {
            case Popular:
                preco = 20000.00;
                break;
            case Basico:
                preco = 35000.00;
                break;
            case SUV:
                preco = 60000.00;
                break;
            case Utilitario:
                preco = 50000.00;
                break;
            case Sedan:
                preco = 45000.00;
                break;
            default:
                preco = 0.0;
                break;
        }
        return preco;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public ETipoCarro getTipo() {
        return tipo;
    }

    public void setTipo(ETipoCarro tipo) {
        this.tipo = tipo;
    }

    public boolean getAr() {
        return ar;
    }

    public void setAr(Boolean ar) {
        this.ar = ar;
    }

    public boolean getCambioAutomatico() {
        return cambioAutomatico;
    }

    public void setCambioAutomatico(Boolean cambioAutomatico) {
        this.cambioAutomatico = cambioAutomatico;
    }

    public boolean getAlarme() {
        return alarme;
    }

    public void setAlarme(Boolean alarme) {
        this.alarme = alarme;
    }

    public boolean getPinturaEspecial() {
        return pinturaEspecial;
    }

    public void setPinturaEspecial(Boolean pinturaEspecial) {
        this.pinturaEspecial = pinturaEspecial;
    }

    public boolean getPinturaMetalica() {
        return pinturaMetalica;
    }

    public void setPinturaMetalica(Boolean pinturaMetalica) {
        this.pinturaMetalica = pinturaMetalica;
    }

    public boolean getPinturaComemorativa() {
        return pinturaComemorativa;
    }

    public void setPinturaComemorativa(Boolean pinturaComemorativa) {
        this.pinturaComemorativa = pinturaComemorativa;
    }

    public boolean getTetoSolar() {
        return tetoSolar;
    }

    public void setTetoSolar(Boolean tetoSolar) {
        this.tetoSolar = tetoSolar;
    }

    public boolean getKitMultimidia() {
        return kitMultimidia;
    }

    public void setKitMultimidia(Boolean kitMultimidia) {
        this.kitMultimidia = kitMultimidia;
    }

    public boolean getMotor1x0() {
        return motor1x0;
    }

    public void setMotor1x0(Boolean motor1x0) {
        this.motor1x0 = motor1x0;
    }

    public boolean getMotor1x6() {
        return motor1x6;
    }

    public void setMotor1x6(Boolean motor1x6) {
        this.motor1x6 = motor1x6;
    }

    public boolean getMotor2x0() {
        return motor2x0;
    }

    public void setMotor2x0(Boolean motor2x0) {
        this.motor2x0 = motor2x0;
    }

    public boolean getIsImportado() {
        if(getTipo() == ETipoCarro.SUV || getTipo() == ETipoCarro.Sedan)
            return isImportado = true;
        
        return isImportado;
    }

    public double valorFinal() {
        Double total = (calculaOpcionais() + getPreco());
        total += (total * calculaImposto());
        
        return total;
    }

    private double calculaOpcionais() {
        Double custo = 0.0;
        if (getAr()) {
            custo += 3000.00;
        }
        if (getCambioAutomatico()) {
            custo += 5000.00;
        }
        if (getAlarme()) {
            custo += 800.00;
        }
        if (getPinturaEspecial() || getPinturaMetalica() || getPinturaComemorativa()) {
            custo += 2500.00;
        }
        if (getTetoSolar()) {
            custo += 4000.00;
        }
        if (getKitMultimidia()) {
            custo += 1800.00;
        }

        return custo;
    }

    private double calculaImposto() {
        Double taxa = 0.2;

        if (getIsImportado()) {
            return taxa += 0.3;
        } else if (getMotor1x0()) {
            return taxa -= 0.1;
        } else if (getIsImportado() && getMotor1x0()) {
            return taxa += (0.3 - 0.1);
        }else{
            return taxa;
        }
    }
}
