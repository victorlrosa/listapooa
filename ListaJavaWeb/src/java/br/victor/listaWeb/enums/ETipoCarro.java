/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package br.victor.listaWeb.enums;

/**
 *
 * @author Victor
 */
public enum ETipoCarro {
    Popular, Basico, SUV, Utilitario, Sedan
}
